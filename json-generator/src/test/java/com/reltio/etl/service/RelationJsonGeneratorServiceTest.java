package com.reltio.etl.service;

import com.bazaarvoice.jolt.JsonUtil;
import com.bazaarvoice.jolt.JsonUtilImpl;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;
import java.util.Map;

/**
 * Created by vignesh on 23/5/17.
 */
public class RelationJsonGeneratorServiceTest {

    private JsonUtil jsonUtil = new JsonUtilImpl();


    @Test
    public void relationJsonGenerationWithSourceTable() throws Exception {

        String[] filePath = {"src/test/resources/relation/relationJsonGenerationWithSourceTable-config.properties"};
        RelationJsonGeneratorService.main(filePath);
        Thread.sleep(100);
        Object inputJson = jsonUtil.classpathToObject(
                "/relation/relationJsonGenerationWithSourceTable-expected.json");
        Object expectedJson = jsonUtil.classpathToObject(
                "/relation/relationJsonGenerationWithSourceTable-json-src1.json");

        Assert.assertEquals(inputJson, expectedJson);
    }

    @Test
    public void relationJsonGenerationWithLudAndCreateDate() throws Exception {

        String[] filePath = {"src/test/resources/relation/relationJsonGenerationWithLUDandCreateDate-config.properties"};
        RelationJsonGeneratorService.main(filePath);
        Thread.sleep(100);
        Object inputJson = jsonUtil.classpathToObject(
                "/relation/relationJsonGenerationWithLUDandCreateDate-json-src1.json");
        Object expectedJson = jsonUtil.classpathToObject(
                "/relation/relationJsonGenerationWithLUDandCreateDate-json-expected.json");

        Assert.assertEquals(inputJson, expectedJson);
    }

    @Test
    public void relationJsonGenerationWithSourceTableIgnoreRelCrosswalk() throws Exception {

        String[] filePath = {"src/test/resources/relation/relationJsonGenerationWithSourceTableIgnoreRelationCrosswalk-config.properties"};
        RelationJsonGeneratorService.main(filePath);
        Object inputJson = jsonUtil.classpathToObject(
                "/relation/relationJsonGenerationWithSourceTableIgnoreRelationCrosswalk-expected.json");
        Object expectedJson = jsonUtil.classpathToObject(
                "/relation/relationJsonGenerationWithSourceTable-json-ignoreRelCrosswalk-src.json");

        Assert.assertEquals(inputJson, expectedJson);
    }


    @Test
    @SuppressWarnings("unchecked")
    public void relationJsonGeneAZ() throws Exception {

        String[] filePath = {"src/test/resources/az/config.properties"};
        Object expectedCrosswalk = jsonUtil.classpathToObject("/az/excpetedCrosswalk.json");
        Object expectedAttribute = jsonUtil.classpathToObject("/az/expectedAttribute.json");
        RelationJsonGeneratorService.main(filePath);
        Thread.sleep(100);
        List<Object> inputJson = jsonUtil.classpathToList("/az/Json_RelationWithSplChar.json");
        for (Object singleObject : inputJson) {
            Map<String, Object> singleObjectMap = (Map<String, Object>) singleObject;
            if (singleObjectMap.get("crosswalks").equals(expectedCrosswalk)) {
                Assert.assertEquals(expectedAttribute, singleObjectMap.get("attributes"));
                break;
            }
        }
    }

    @Test
    public void relationJsonGenerationWithoutNull() throws Exception {

        String[] filePath = {"src/test/resources/relation/relationJsonGenerationWithoutNull-config.properties"};
        RelationJsonGeneratorService.main(filePath);
        Object inputJson = jsonUtil.classpathToObject(
                "/relation/relationJsonGenerationWithoutNull-json-expected.json");
        Object expectedJson = jsonUtil.classpathToObject(
                "/relation/relationJsonGenerationWithoutNull-json-src.json");

        Assert.assertEquals(inputJson, expectedJson);
    }

    @Test
    public void relationJsonGenerationWithNull() throws Exception {

        String[] filePath = {"src/test/resources/relation/relationJsonGenerationWithNull-config.properties"};
        RelationJsonGeneratorService.main(filePath);
        Object inputJson = jsonUtil.classpathToObject(
                "/relation/relationJsonGenerationWithNull-json-expected.json");
        Object expectedJson = jsonUtil.classpathToObject(
                "/relation/relationJsonGenerationWithNull-json-src.json");

        Assert.assertEquals(inputJson, expectedJson);
    }


    @Test
    public void bugTestCUST3030() throws Exception {
        String[] filePath = {"src/test/resources/bug.CUST-3030/config.properties"};
        RelationJsonGeneratorService.main(filePath);
        Object inputJson = jsonUtil.classpathToObject(
                "/bug.CUST-3030/output.json");
        Object expectedJson = jsonUtil.classpathToObject(
                "/bug.CUST-3030/output-expected.json");

        Assert.assertEquals(inputJson, expectedJson);


    }
    
    
    @Test
    public void MultiJsonGenerateWithNull() throws Exception {
        String[] filePath = {"src/test/resources/relation/MultiJsonGenerateWithNull/config.properties"};
        RelationJsonGeneratorService.main(filePath);
        Thread.sleep(100);
        Object inputJson = jsonUtil.classpathToObject(
                "/relation/MultiJsonGenerateWithNull/output.json");
        Object expectedJson = jsonUtil.classpathToObject(
                "/relation/MultiJsonGenerateWithNull/output-expected.json");

        Assert.assertEquals(inputJson, expectedJson);

    }
    
    @Test
    public void SingleJsonGenerateWithNull() throws Exception {
        String[] filePath = {"src/test/resources/relation/SingleJsonGenerateWithNull/config.properties"};
        RelationJsonGeneratorService.main(filePath);
        Thread.sleep(100);
        Object inputJson = jsonUtil.classpathToObject(
                "/relation/SingleJsonGenerateWithNull/output.json");
        Object expectedJson = jsonUtil.classpathToObject(
                "/relation/SingleJsonGenerateWithNull/output-expected.json");

        Assert.assertEquals(inputJson, expectedJson);
    }
    
    @Test
    public void SingleJsonGenerateWithoutNull() throws Exception {
        String[] filePath = {"src/test/resources/relation/SingleJsonGenerateWithoutNull/config.properties"};
        RelationJsonGeneratorService.main(filePath);
        Thread.sleep(100);
        Object inputJson = jsonUtil.classpathToObject(
                "/relation/SingleJsonGenerateWithoutNull/output.json");
        Object expectedJson = jsonUtil.classpathToObject(
                "/relation/SingleJsonGenerateWithoutNull/output-expected.json");

        Assert.assertEquals(inputJson, expectedJson);
    }
    
    @Test
    public void MultiJsonGenerateWithoutNull() throws Exception {
        String[] filePath = {"src/test/resources/relation/MultiJsonGenerateWithoutNull/config.properties"};
        RelationJsonGeneratorService.main(filePath);
        Thread.sleep(100);
        Object inputJson = jsonUtil.classpathToObject(
                "/relation/MultiJsonGenerateWithoutNull/output.json");
        Object expectedJson = jsonUtil.classpathToObject(
                "/relation/MultiJsonGenerateWithoutNull/output-expected.json");

        Assert.assertEquals(inputJson, expectedJson);
    }
}